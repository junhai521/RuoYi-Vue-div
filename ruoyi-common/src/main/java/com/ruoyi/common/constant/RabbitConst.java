package com.ruoyi.common.constant;

/**
 * RabbitMQ 常量类
 *
 * @author junhaivip@163.vom
 * @date 2024/03/16
 */
public class RabbitConst {
    /**
     * 交换机
     */
    public static final String DELAY_EXCHANGE = "delay_exchange";

    /**
     * 队列
     */
    public static final String DELAY_QUEUE = "delay_queue";

    /**
     * 路由
     */
    public static final String DELAY_KEY = "delay_key";

}
